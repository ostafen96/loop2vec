import json
import argparse
import random
import torch
import torch.nn as nn
from torch.optim import SGD
from torchnet.logger import VisdomPlotLogger, VisdomSaver
from torchnet.meter import AverageValueMeter
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from code2vec import Code2Vec

DEFAULT_DEVICE = 'cpu'
DEFAULT_EPOCHS = 50

argparse = argparse.ArgumentParser()
argparse.add_argument('dataset', type=str, help='Filename of the json input dataset.')
argparse.add_argument('--device', '-d', type=str, choices=['cpu', 'cuda'], nargs='?', default=DEFAULT_DEVICE, help='Device to use for training.')
argparse.add_argument('--epochs', '-e', type=int, nargs='?', default=DEFAULT_EPOCHS, help='Number of epochs to perform.')
args = argparse.parse_args()

filename = args.dataset

def load_dataset(filename):
    labels = []
    records = []
    with open(filename, 'r') as json_file:
        json_object = json.load(json_file)
        
        for obj in json_object:
            label = int(obj['isParallel'])
            contexts = [[c['left'], c['path'], c['right']] for c in obj['serializers']]
            labels.append(label)
            records.append(contexts)
        return records, labels

def pad_contexts(contexts):
    max_len = len(max(contexts))

    out = []
    for ctx in contexts:
        new_contexts = None
        if len(ctx) < max_len:
            padded_contexts = ctx
            for _ in range(max_len-len(ctx)):
                padded_contexts.append(['<pad>', '<pad>', '<pad>'])
            new_contexts = padded_contexts
        else:
            new_contexts = ctx
        out.append(new_contexts)
    return out


# create vocabularies, initialized with unk and pad tokens

word2idx = {'<unk>': 0, '<pad>': 1}
path2idx = {'<unk>': 0, '<pad>': 1}

data, labels = load_dataset(filename)
data = pad_contexts(data)

train_data, test_data = train_test_split(list(zip(data, labels)), test_size=0.25)

train_data, train_labels = zip(*train_data)
test_data, test_labels = zip(*test_data)

MAX_LEN = 200
EMBEDDING_DIM = 32
DROPOUT = 0.25
BATCH_SIZE = 10
N_EPOCHS = 50

def create_vocab(data):
    word2idx = {'<unk>': 0, '<pad>': 1}
    path2idx = {'<unk>': 0, '<pad>': 1}

    for ctxs in data:
        for [left, path, right] in ctxs:
            if left not in word2idx:
                word2idx[left] = len(word2idx)
            
            if right not in word2idx:
                word2idx[right] = len(word2idx)
            
            if path not in path2idx:
                path2idx[path] = len(path2idx)

    return word2idx, path2idx

def chunks(seq, size):
    return [seq[i:(i+size)] for i in (range(0, len(seq), size) if len(seq) >= size else range(1))]

word2idx, path2idx = create_vocab(data)


def iterate_batch(data, labels):
    for batch in chunks(list(zip(data, labels)), BATCH_SIZE):
        
        tensor_cl = torch.zeros(BATCH_SIZE)
        tensor_l = torch.zeros(BATCH_SIZE, MAX_LEN).long()
        tensor_p = torch.zeros(BATCH_SIZE, MAX_LEN).long()
        tensor_r = torch.zeros(BATCH_SIZE, MAX_LEN).long()

        for i, record in enumerate(batch):
            contexts, label = record
            left_nodes, paths, right_nodes = zip(*contexts)

            left_idxs = [word2idx.get(l, word2idx['<unk>']) for l in left_nodes]
            path_idxs = [path2idx.get(p, path2idx['<unk>']) for p in paths]
            right_idxs = [word2idx.get(r, word2idx['<unk>']) for r in right_nodes]
            
            tensor_cl[i] = label
            tensor_l[i, :] = torch.Tensor(left_idxs).long()
            tensor_p[i, :] = torch.Tensor(path_idxs).long()
            tensor_r[i, :] = torch.Tensor(right_idxs).long()
            
        yield tensor_cl, tensor_l, tensor_p, tensor_r


model = Code2Vec(len(word2idx), len(path2idx), EMBEDDING_DIM, DROPOUT)
criterion = nn.BCELoss()
optimizer = SGD(model.parameters(), lr=0.001, momentum=0.9)

def train_model(model, epochs, optimizer, criterion, device):

    loss_meter = AverageValueMeter()
    acc_meter = AverageValueMeter()
    
    loss_logger = VisdomPlotLogger('line', env='code2vec', opts={'title': 'Loss', 'legend':['train','test']})
    
    acc_logger = VisdomPlotLogger('line', env='code2vec', opts={'title': 'Accuracy','legend':['train','test']})

    visdom_saver = VisdomSaver(envs=['code2vec'])

    for e in range(epochs):
        #iteriamo tra due modalità: train e test
        for mode in ['train', 'test'] :
            loss_meter.reset(); acc_meter.reset()
            model.train() if mode == 'train' else model.eval()

            with torch.set_grad_enabled(mode=='train'):
                
                data = train_data if mode == 'train' else test_data
                labels = train_labels if mode == 'train' else test_labels

                for i, (tensor_cl, tensor_l, tensor_p, tensor_r) in enumerate(iterate_batch(data, labels)):
                    output = model(tensor_l, tensor_p, tensor_r)
                    l = criterion(output, tensor_cl)

                    if mode=='train':
                        l.backward()
                        optimizer.step()
                        optimizer.zero_grad()

                    acc = accuracy_score(output >= 0.5, tensor_cl.to('cpu'))
                    n = BATCH_SIZE #numero di elementi nel batch
                    loss_meter.add(l.item()*n,n)
                    acc_meter.add(acc*n, n)

                    if mode=='train':
                        loss_logger.log(e+(i+1)/(len(data)/5), loss_meter.value()[0], name=mode)
                        acc_logger.log(e+(i+1)/(len(data)/5), acc_meter.value()[0], name=mode)
            
            loss_logger.log(e+1, loss_meter.value()[0], name=mode)
            acc_logger.log(e+1, acc_meter.value()[0], name=mode)

        torch.save(model.state_dict(),'%s-%d.pth'%('loop2vec', e+1))
    return model

model = train_model(model, args.epochs, optimizer, criterion, args.device)