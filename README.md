# Loop2Vec: Detecting Parallel For Loops

Loop2Vec è una rete neurale basata su Code2Vec (https://github.com/tech-srl/code2vec) che permette di rilevare i cicli for parallelizzabili.

# Requisiti

  - Python3
  - Pytorch >= 1.1.0.
  - visdom server
# Esempio di esecuzione

Una volta ottenuto il file data.json corrispondente al training set in input, per avviare la rete neurale basterà lanciare lo script train.py su di esso nel seguente modo:

```sh
$ python3 train.py data.json
```
Il comando avvierà il training della rete per 50 epoche. Per cambiare il numero di epoche (per esempio aumentandolo a 100), basterà utilizzare il flag -e:
```sh
$ python3 train.py data.json -e 100
```